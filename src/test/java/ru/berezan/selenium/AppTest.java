package ru.berezan.selenium;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    private final Calculator calculator = new Calculator();

    @Test
    public void shouldAnswerWithTrue() {
        assertTrue(true);
    }

    @Test
    public void test(){
        Assert.assertEquals(4,calculator.sum(1,3));
        Assert.assertEquals(-1,calculator.sum(1,-2));
        Assert.assertEquals(900,calculator.sum(1000,-100));

    }

}
