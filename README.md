# SELENIUM EXAMPLE

## DEVELOPER INFO

**e-mail**: berezan4@mail.ru

## SOFT REQUIREMENT

**JAVA**: OPENJDK 1.8
**OS**: WINDOWS 10

## HARDWARE REQUIREMENT

**CPU**: i7
**RAM**: 16 GB
**SSD**: 1024 GB

## BUILD APPLICATION


`mvn clean install`

## RUN TEST

`mvn test`


## BUILD APPLICATION

`java -jar ./p_01-1.0.0.jar`
